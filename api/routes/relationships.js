import express from "express";
import { getRelationship, addRelationships, deleteRelationship } from "../controllers/relationship.js";

const router = express.Router();

router.get('/', getRelationship);
router.post("/", addRelationships);
router.delete("/", deleteRelationship);


export default router;
